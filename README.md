# WHAT IS CDKOMA #

CDKOMA is a Neural Network based method for the exome-based in-silico prediction of Crohn's disease (CD) cases from controls.

More derails will be soon available in:
`Raimondi et al., An interpretable low-complexity Machine Learning framework for robust exome-based in-silico diagnosis of Crohn�s disease patients.`

### What is this repository for? ###

The code here contains a standalone version of CDkoma (`cdkoma.py`), which takes as input an ANNOVAR-annotated `.txt` version of a VCF file and outputs the predicted likelihood of CD. 
Examples of ANNOVAR annotated VCFs are available in `DATA/annovarExamplesdata`.

The file `cdkoma_testOnCAGI2.py` runs a full prediction of the CAGI2 dataset (serialized Annovar annotations of this data are stored in DATA/marshalled for) as an example.
The original data are available at: `https://genomeinterpretation.org/content/crohns-disease-2011` .

### DATA AVAILABILITY ###

We cannot provide all the VCFs data used in the paper due to provacy issues. You can run CDkoma on your exomes.

### How do I set it up? ###

CDkoma has some dependencies. Here we show how to create a miniconda environment containig all those libraries.

* Download miniconda from `https://docs.conda.io/en/latest/miniconda.html`
* Create a new conda environment by typing: `conda create -n cdkoma -python=2.7`
* Enter the environment by typing: `conda activate cdkoma`
* Install pytorch 0.3.1 with the command: `conda install pytorch=0.3.1 -c pytorch`
* Install numpy with the command: `conda install numpy`
	
You can remove this environment at any time by typing: conda remove -n cdkoma --all
 

### What is this repository contains? ###

* `CDkoma.py` -> is the standalone predictor.
* `cdkoma_testOnCAGI2.py` -> Reproduces the validation on the CAGI2 dataset.
* `DATA` -> folder containing data necessary to run the program
* `DATA/annovarExamplesdata` -> folder containing few ANNOVAR-annotated vcfs
* `DATA/databases` -> folder containing the required PhenoPedia CD-associated genes
* `DATA/marshalled` -> folder containing serialized data necessary to run the program
* `DATA/models` -> folder containing the trained NN models.
* `GraphConv.py` -> python code necessary to run the NN
* `README.md` -> this readme
* `sources` -> folder containing python utils necessary to run cdkoma

### How do I annotate exomes with ANNOVAR? ###

These are the links to Annovar documentaiton (`http://annovar.openbioinformatics.org/en/latest/`) and installation instructions (`http://annovar.openbioinformatics.org/en/latest/user-guide/download/`).


To annotate the VCFs from CAGI, we iterated the following command line over each
VCFFILE in the CAGI datasets:

```
perl .../annovar/table annovar.pl VCFFILE .../annovar/humandb/ -out OUTPUT
-vcfinput -buildver hg19 -protocol refGene,dbnsfp33a -operation gx,f -thread
20 -xreffile XREFFILE
```

where OUTPUT is the Annovar file containing the annotations. The database used
for the annotation is dbNSFP version 33a (`https://sites.google.com/site/jpopgen/dbNSFP`).

Examples of the Annovar-annotated files we use as input for CDkoma can be found in `DATA/annovarExamplesdata` .

### How do I compute the prediction for an ANNOVAR-annotated exome? ###

You can run one of our examples by typing:

```
(cdkoma) eddiewrc@alnilam:~/Bioinformatics/cdkoma$ python CDkoma.py 222 DATA/annovarExamplesdata/1.vcf.hg18_multianno.txt 
```

You will obtain the following results:

```
 > Used 222 genes
Found 691 associated genes.
[0]
Vectors with 222 dimensions.
Predicting...
CDkoma predidcted probability-like CD score for DATA/annovarExamplesdata/1.vcf.hg18_multianno.txt: 0.772515 
Activations values have been saved in 1.vcf.hg18_multianno.txt_activations.txt 
```

The final prediction is 0.772515  (and thus likely affected by CD). The per-gene activations are listed in 1.vcf.hg18_multianno.txt_activations.txt

### How do I reproduce (some of) the results of the paper? ###

Besides the `CDkoma.py` standalone predictor, we provide an automated way to predict the CAGI2 dataset. It is not possible to share sequencing data due to privacy concerns.

To run the predictions on the CAGI2 dataset, type the following command line in your terminal (once the cdkoma conda env is active!)

```
(cdkoma) eddiewrc@alnilam:~/Bioinformatics/cdkoma$ python cdkoma_testOnCAGI2.py 222
```

You will get the following results:

```
 > Used 222 genes
Found 691 associated genes.
Testing DATA/marshalled/cagi2.multianno.missenseFalse.RegionsNone.m.min1 ######################################################################################################
Marshalling time:  0.0385389328003
Vectors with 222 dimensions.
Predicting...
Computing scores for 56 predictions
 > Best threshold: 0.7691258788108826
      | DEL         | NEUT             |
DEL   | TP: 42   | FP: 6  |
NEUT  | FN: 1   | TN: 8  |

Sen = 0.977
Spe = 0.571
Acc = 0.877 
Bac = 0.774
Pre = 0.875
MCC = 0.647
AUC = 0.716
AUPRC= 0.837
--------------------------------------------
Best 5 precision: 0.800
Best 10 precision: 0.800
Best 20 precision: 0.750
Best 30 precision: 0.833
```

### Who do I talk to? ###
daniele DoT raimondi aT kuleuven DoTbe

yves dOt moreau At kuleuven dOt be
