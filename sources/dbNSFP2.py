#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it, daniele.raimondi@vub.ac.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
verbosity = 2
import numpy as np
import math

def readDbNSFP(filename):
	ifp = open(filename, "r")
	db = {}
	ifp.readline()
	line = ifp.readline()
	while len(line) > 1:
		tmp = line.strip().split("\t")
		uid = tmp[5]
		pathwayConsPath = tmp[16]
		if tmp[23] == ".":
			traitAssoc = 0
		else:
			traitAssoc = len(tmp[23].split(";"))
		
		tissSpec = tmp[27]
		
		if tmp[28] == ".":
			exprEgen = 0
		else:
			exprEgen = len(tmp[28].split(";"))	
					
		if tmp[29] == ".":
			exprAtl = 0
		else:
			exprAtl = len(tmp[29].split(";"))	
		#print exprEgen, exprAtl		
		
		#raw_input()
		meanExpr = []
		i = 32
		while i < 241:
			if tmp[i] == ".":
				tmp[i] = 0.0
			meanExpr.append(float(tmp[i]))
			#meanExpr.append(math.log(max(0.0000001, float(tmp[i]))))
			i+=4
		#print meanExpr
		#raw_input()	
		if tmp[244] == ".":
			intConsPath = 0
		else:
			intConsPath = int(tmp[244])
		
		if tmp[245] == ".":
			HI = 0
		else:
			HI = float(tmp[245])
			
		if tmp[246] == ".":
			rec = 0
		else:	
			rec = float(tmp[246])
		#print rec
		
	
		if tmp[248] == ".":
			RVIS = 0
		else:			
			RVIS = float(tmp[248])
			
		if tmp[252] == ".":
			GDI = 0
		else:			
			GDI = float(tmp[252])
			
		if tmp[253] == ".":
			GDIscaled = 0
		else:		
			GDIscaled = float(tmp[253])
			
		if tmp[265] == ".":
			ESS = -1
		else:
			if 	tmp[265] == "N":
				ESS = 0
			else:
				ESS = 1
		db[uid] = [pathwayConsPath, traitAssoc, tissSpec, exprEgen, exprAtl, sum(meanExpr)/float(len(meanExpr)), intConsPath, HI, rec, RVIS, GDI, GDIscaled, ESS]
		
		line = ifp.readline()
	
	
	return db # indelDb = {UID:[(ind, class), (ind, class), ]}


def main():
	db = readDbNSFP("../databases/DBNSFP3.1/dbNSFP3.1_gene")
	print len(db.keys())
	return 0

if __name__ == '__main__':
	main()

