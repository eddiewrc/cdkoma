#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import re, math
verbosity = 2
aaCodes = {"sec":"U",'ala':"A", 'arg':"R", 'asn':"N", 'asp':"D", 'cys':"C", 'glu':"E", 'gln':"Q", 'gly':"G", 'his':"H", 'ile':"I", 'leu':"L", 'lys':"K", 'met':"M", 'phe':"F", 'pro':"P", 'ser':"S", 'thr':"T", 'trp':"W", 'tyr':"Y", 'val':"V"}
aleph = ['C', 'P', 'H', 'D', 'S', 'Q', 'I', 'M', 'K', 'T', 'F', 'A', 'G', 'E', 'L', 'R', 'W', 'N', 'Y', 'V']

def readPROVEAN(filename):
	ifp = open(filename, "r")
	prov = ifp.readlines()	
	ifp.close()	
	print prov.pop(0).split()
	provDB = {}
	neut = 0
	dis = 0
	for row in prov:
		line = row.split()
		#print line	
		if not provDB.has_key(line[0]):
			provDB[line[0]] = {}
		provDB[line[0]][line[1]] = float(line[2])
		if line[-1] == "Polymorphism":
			neut += 1
		elif line[-1] == "Disease":
			dis += 1
		else:
			raise Exception("Expected disease or poy here!")
	print "Found %d poly and %d disease" % (neut, dis)
	return provDB #{UID:[mutm score, class]}


def readHumsavar(filename):
	ifp = open(filename, "r")
	humsavar = {}
	uids = set()
	if verbosity >= 2:
		print "Reading mutations from %s..." % (filename)		
	i = 0
	line = ifp.readline()
	delet = 0
	poly = 0
	
	while len(line) != 0:
		match = re.search(r"^.+\s*(\w{6,6})\s*(VAR_\d{6,6})\s*p\.(\w{3,3}\d*\w{3,3})\s*(\w+).*\n", line)
		if match == None:
			line = ifp.readline()
			continue
		uid = line[match.start(1):match.end(1)]
		var = line[match.start(2):match.end(2)]
		mut = line[match.start(3):match.end(3)]
		mclass = line[match.start(4):match.end(4)]
		if mclass != "Unclassified":
			if not humsavar.has_key(uid):
				humsavar[uid] = []
			humsavar[uid].append((mut,mclass))
			if verbosity >= 3:
				print uid,
				print var,
				print mut,
				print mclass
			if mclass == "Polymorphism":
				poly += 1
			else:
				delet += 1
			uids.add(uid)
		line = ifp.readline()		
	print "Poly: ", poly
	print "Dele: ", delet
	#marshal.dump(humsavar, open("humsavar.m","w"))
	return humsavar # humsavar = {UID:[(mut, class), (mut, class), ]}

def translateMutation(mutation): #gap aware
	match = re.search(r"^(\w{3})(\d+)(\w{3})", mutation)
	wild = mutation[match.start(1):match.end(1)].lower()
	pos = int(mutation[match.start(2):match.end(2)])-1
	mut = mutation[match.start(3):match.end(3)].lower()
	return aaCodes[wild], pos, aaCodes[mut]

def readDynaFormat(f, db = {}):
	if verbosity >= 2:
		print "Reading EF predictions from %s..." % (f)
	PRINT = False
	
	ifp = open(f)
	lines = ifp.readlines()
	ifp.close()
	tmp = []
	name = ""
	l = 0
	
	while l < len(lines):
		if lines[l][0] == "*":
			name = lines[l+4][6:40].strip()[3:9]
			#print name
			#raw_input()
			l+=11
		assert lines[l] != "*"
		while l < len(lines) and lines[l][0] != "*":
			if lines[l][0] == " ":
				l+=1
				continue
			#print lines[l][0]
			tmp.append(float(lines[l][3:].strip()))			
			l+=1		
		db[name] = tmp
		tmp = []
		
		assert "" not in db.keys()
		
	if PRINT == True:
		for p in db.items():
			name = p[0]
			tmp = p[1]
			DIR = "ppIIDir/"
			os.system("mkdir  -p "+DIR)
			plt.plot(tmp)
			plt.title(name)
			plt.savefig(DIR+name+".png",dpi=300)
			plt.clf()
	return db # {UID:[ef preds1, ef pred2, ...]}

def readPFAMScan(f):
	ifp = open(f, "r")
	line = ifp.readline()
	pfamdb = {}
	#print "Start"
	while len(line) > 0:
		#print line
		if line[0] == "#" or len(line) == 1:
			line = ifp.readline()
			continue
		tmp = line.strip().split(" ")
		i = 0#remove spaces!
		while i < len(tmp):
			if tmp[i] == "":
				tmp.pop(i)
				continue
			i+=1
			 
		#print tmp
		uid = tmp[0][3:9]
		start = int(tmp[1])
		end = int(tmp[2])
		typ = tmp[7]
		name = tmp[6]
		acc = tmp[5]
		if len(tmp) == 16:
			asite = readASite(tmp[-1])
			#raw_input()
		else:
			asite = []
		tmpv = [start, end, typ, name, acc, asite]
		#print tmpv
		if not pfamdb.has_key(uid):
			pfamdb[uid] = []
		pfamdb[uid].append(tmpv)
		line = ifp.readline()
		
	print "Read pfamn for %d proteins" % len(pfamdb.keys())
	return pfamdb
	
def readASite(txt):
	start = txt.index("[")
	end = txt.index("]")
	text = txt[start+1:end]
	#print text
	tmp = text.split(",")
	pos = []
	for i in tmp:
		pos.append(int(i))
	return pos	
	
def coverage(s1, s2):
	return len(s2.replace("-",""))/float(len(s1.replace("-","")))	
		
def getOrganism(s):
	try:
		start = s.index("OS=")+3
	except:
		return None
	name = ""
	i = start
	while i < len(s):
		name += s[i]
		if s[i] == "=":
			return name[:-3]
		if s[i] == "\n":
			return name[-1]
		i+=1
	return None
	
def readMSANoFilt(alName):
	ifp = open(alName, "rb")
	msa = ifp.readlines()
	ifp.close()
	if len(msa) == 0:
		return msa
	if verbosity >= 3:
		print " > Read %d sequences." % len(msa)
	i = 0
	while i < len(msa):
		msa[i] = msa[i].strip()
		i += 1	
	return msa	

def getScoresSVR(pred, real, threshold):
	if len(pred) != len(real):
		raise Exception("ERROR: input vectors have differente len!")
	i = 0
	confusionMatrix = {}
	confusionMatrix["TP"] = confusionMatrix.get("TP", 0)
	confusionMatrix["FP"] = confusionMatrix.get("FP", 0)
	confusionMatrix["FN"] = confusionMatrix.get("FN", 0)
	confusionMatrix["TN"] = confusionMatrix.get("TN", 0)
	while i < len(real):
		if float(pred[i])<threshold and (real[i]==0):
			confusionMatrix["TN"] = confusionMatrix.get("TN", 0) + 1
		if float(pred[i])<threshold and real[i]==1:
			confusionMatrix["FN"] = confusionMatrix.get("FN", 0) + 1
		if float(pred[i])>threshold and real[i]==1:
			confusionMatrix["TP"] = confusionMatrix.get("TP", 0) + 1
		if float(pred[i])>threshold and real[i]==0:
			confusionMatrix["FP"] = confusionMatrix.get("FP", 0) + 1
		i += 1
	#print "--------------------------------------------"
	#print confusionMatrix["TN"],confusionMatrix["FN"],confusionMatrix["TP"],confusionMatrix["FP"]
	'''
	print "      | DEL         | NEUT             |"
	print "DEL   | TP: %d   | FP: %d  |" % (confusionMatrix["TP"], confusionMatrix["FP"] )
	print "NEUT  | FN: %d   | TN: %d  |" % (confusionMatrix["FN"], confusionMatrix["TN"])	
	'''
	sen = (confusionMatrix["TP"]/float((confusionMatrix["TP"] + confusionMatrix["FN"])))
	spe = (confusionMatrix["TN"]/float((confusionMatrix["TN"] + confusionMatrix["FP"])))
	acc =  (confusionMatrix["TP"] + confusionMatrix["TN"])/float((sum(confusionMatrix.values())))
	bac = (0.5*(sen+spe))	
	pre =(confusionMatrix["TP"]/float((confusionMatrix["TP"] + confusionMatrix["FP"])))
	mcc =	( ((confusionMatrix["TP"] * confusionMatrix["TN"])-(confusionMatrix["FN"] * confusionMatrix["FP"])) / math.sqrt((confusionMatrix["TP"]+confusionMatrix["FP"])*(confusionMatrix["TP"]+confusionMatrix["FN"])*(confusionMatrix["TN"]+confusionMatrix["FP"])*(confusionMatrix["TN"]+confusionMatrix["FN"])) )  
	#from sklearn.metrics import roc_curve, auc
	#fpr, tpr, thresholds = roc_curve(pred, real)
	#aucScore = auc(fpr, tpr)
	from sklearn.metrics import roc_auc_score
	aucScore = roc_auc_score(real, pred)
	
	print "\nSen = %3.3f" % sen
	print "Spe = %3.3f" %  spe
	print "Acc = %3.3f " % acc
	print "Bac = %3.3f" %  bac
	#print "Inf = %3.3f" % inf
	print "Pre = %3.3f" %  pre
	print "MCC = %3.3f" % mcc
	print "AUC = %3.3f" % aucScore
	print "--------------------------------------------"	
	
	return sen, spe, acc, bac, pre, mcc, aucScore


def retrieveUID(l):
	uidList = []
	for i in l:
		ifp = open(i, "r")
		lines = ifp.readlines()
		for j in lines:
			uidList.append(j.strip())		
	if verbosity >= 2:
		print "Num of uid retrieved: " + str(len(uidList))
	return uidList	

def readSubset(filename):
	ifp = open(filename, "r")
	if verbosity >= 2:
		print "Reading subset from %s..." % (filename)	
	uidList = []
	line = ifp.readline()
	while len(line) != 0:
		uidList.append(line.strip())
		line = ifp.readline()
	print "Red %d uids." % len(uidList)
	return uidList

def getProbMut(msa, pos, mut, freqsTot):
	aleph = ['C', 'P', 'H', 'D', 'S', 'Q', 'I', 'M', 'K', 'T', 'F', 'A', 'G', 'E', 'L', 'R', 'W', 'N', 'Y', 'V']	
	if pos > len(msa[0]):
		print "WARNING: position out of the sequence!"
		return [0,0]
	aaAtPos = []
	#print pos, mut
	for seq in msa:
		if seq[pos] in aleph:#dUtils.aaCodes.values():
			#aaAtPos.append(dUtils.inExt[seq[pos]])
			aaAtPos.append(seq[pos])
	tot = float(len(msa))+20
	#aleph = set(dUtils.aaCodes.values())
	assert len(aleph) == 20
	freqs = {}
	for aa in aleph:
		freqs[aa] = (aaAtPos.count(aa)+1)/tot
	assert sum(freqs.values()) -1.0 < 0.1	

	freqMut = freqs[mut[1]]	
	assert freqMut > 0
	CI = computeCI(freqs, freqsTot, aleph)	
	return CI, math.log((freqs[mut[0]]/(1.0-freqs[mut[0]]))/(freqs[mut[1]]/(1.0-freqs[mut[0]])) )

def computeMSAFreqs(msa, aleph):
	concatMSA = "".join(msa)
	tot = float(len(concatMSA))+20
	freqsTot = {}
	for aa in aleph:
		freqsTot[aa] = (concatMSA.count(aa)+1)/tot
	assert sum(freqsTot.values()) -1.0 < 0.1
	return freqsTot

def computeCI(freqs, freqsTot, aleph):
	ci = 0.0
	assert len(aleph) == 20
	for i in aleph:			
		ci += (freqs[i] - freqsTot[i])**2
	return math.sqrt(ci)	
	
def main():
	#a = readHumsavar("../humsavar02_2016/humsavar.txt")
	#ofp = open("../humsavar02_2016/humsavar.fasta","w")
	#for i in a.keys():
	#	ofp.write(i+"\n")
	#ofp.close()	
	#db = readDynaFormat("../databases/EF/husavar_earlyFold.pred")
	db = readPFAMScan("../databases/PFAM/PfamScan/humsavar16.pfamScan")
	
if __name__ == '__main__':
	main()

