#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  readPhenopedia.py
#  
#  Copyright 2017 Daniele Raimondi <daniele.raimondi@vub.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

def readGWASHist(f):
	ifp = open(f)
	lines = ifp.readlines()
	print lines.pop(0)
	db = {}
	for l in lines:
		tmp = l.strip().split("\t")
		#print tmp
		#raw_input()
		chrom = crom2int(tmp[0])		
		start = int(tmp[1])
		end = int(tmp[2])
		count = int(tmp[4])
		if not db.has_key(chrom):
			db[chrom] = []
		db[chrom].append((start, end, count))	
	print db.keys()
	return db

def getHistCount(crom, pos, db):
	c = db[crom]
	#print c
	count = 0
	for r in c:
		if pos < r[1] and pos >= r[0]:
			return r[2]
	if pos > c[-1][1]:
		return c[-1][2]		
	#print "WARNING !", crom, pos
	return 1

def crom2int(s):
	try:
		return int(s)
	except:
		pass
	if s == "Y":
		return 23
	elif s == "X":
		return 24
	raise Exception("%s is not a valid cromosome name" % s)
	
def main():
	db = readGWASHist("../../databases/gwasCentral/gwasCDstudies.hist.csv")
	print getHistCount(1, 111111111, db)
	

if __name__ == '__main__':
	main()
