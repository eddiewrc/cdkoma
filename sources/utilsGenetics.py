#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  utils.py
#  
#  Copyright 2017 Daniele Raimondi <eddiewrc@mira>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import scipy.stats, random, math, copy
import numpy as np

def buildFeatVect(HX, Ydb, samples):
	X = []
	Y = []	
	for s in samples:
		Y.append(Ydb[s])
		#print HX[s]		
		#X.append(iDCTquantization(vectorizeConcept(HX[s]), LEN=20))
		X.append(vectorizeConcept(HX[s], EXPECTED_LEN = 4))
	assert len(X) == len(Y)
	return X, Y
	
def vectorizeConcept(cv, EXPECTED_LEN):
	x = []
	for i in cv:
		if i != None and len(i) == EXPECTED_LEN:
			x += i
		else:
			x += copy.deepcopy([0.0]*EXPECTED_LEN)
	assert len(x) == EXPECTED_LEN * len(cv)

	return x

def buildVectorGeneWise(genedb, geneList):	
	vect = []
	for k in geneList:
		varList = genedb[k]
		if len(varList) < 1:
			vect.append(None)
			continue
		vect.append([]+getVarEffPreds(varList,13)+getVarEffPreds(varList,14))#+getGeneScores(varList))
	return vect, len(vect[0])

def getGeneScores(vl):
	if len(vl) > 0:
		return [vl[0][10], vl[0][11], vl[0][12]]
	else:
		return [0,0,0]
			
def getVarEffPreds(vl, pos):
	scores = []
	for v in vl:
		if v[pos] != None:
			scores.append(v[pos])
	if len(scores) > 0:
		return [np.mean(scores), max(scores)]		
	else:
		return [0,0]	
				
def scanGenes(exome, geneList):
	geneDB = {}
	for g in sorted(geneList):
		geneDB[g] = []
	for crom in exome:			
		cromName = crom[0]
		varList = crom[1]			
		for var in varList:	
			geneDB[var[3]].append(var)
	return geneDB #list of variants foreach gene

def main():

	return 0

if __name__ == '__main__':
	main()
