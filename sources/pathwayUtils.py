#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it, daniele.raimondi@vub.ac.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import math

def label(cl):
	if cl == "Polymorphism":
		return 0
	elif cl == "Disease":
		return 1
	else:
		raise Exception("Expected disease or poly here, got: "+cl)


class Pathways:
	def __init__(self, uniprotFile, pathwaysFile):
		self.uniprotDB = self.readUniprotTab(uniprotFile)	#{UID: [GENE, SUCELL, DOMAIN]}		
		self.pathGENEwise = self.readPathwaysGENEwise(pathwaysFile, self.uniprotDB) # {GENE:[pathways, p2 ,p3]}	
		#print self.pathGENEwise.items()[:4]
		self.pathIDwise = self.readPathwaysIDwise(pathwaysFile, self.uniprotDB)
		#print self.pathIDwise.items()[:4]
		#self.merged = self.mergeDBs() # {UID:[pathways]}
		#assert len(self.merged.keys()) == len(self.uniprotDB.keys())

	def sharePath(self, g):
		rg = set()
		tmpPath = []
		try:
			tmpPath = self.pathGENEwise[g]
		except:
			#print "No pathways found"
			return rg
		for p in tmpPath:
			tmp = self.pathIDwise[p]
			#print tmp			
			for i in tmp[1]:
				rg.add(i)
		return rg

	def readUniprotTab(self, f):
		ifp = open(f,"r")
		uniprotDB = {}
		ifp.readline()
		lines = ifp.readlines()
		for line in lines:
			tmp = line.strip().split("\t")		
			#print tmp
			if len(tmp) == 1:
				continue
			if not uniprotDB.has_key(tmp[0]):
				uniprotDB[tmp[0]] = []
			uniprotDB[tmp[0]] = parseGenes(tmp[1])
			#uniprotDB[tmp[0]] = [parseGenes(tmp[1]),parseSubcell(tmp[2]),parseDomains(tmp[3])] #{UID: [GENE, SUCELL, DOMAIN]}
			#raw_input()		
		return uniprotDB
	
	def readPathwaysIDwise(self, f, uniprotDB):
		ifp = open(f,"r")	
		pathwaysDB = {}	
		ifp.readline()
		lines = ifp.readlines()
		for line in lines:
			tmp = line.strip().split("\t")
			#print tmp
			if tmp[1] == "None":
				name = tmp[0]
			else:
				name = tmp[1]
			if not pathwaysDB.has_key(name):
				pathwaysDB[name] = []
			pathwaysDB[name] = [tmp[0], parseGenesUniprot(tmp[3], uniprotDB),tmp[2]] #{UID: [DESCRIPTION, GENES, SOURCE]}		
		return pathwaysDB
	
	def readPathwaysGENEwise(self, f, uniprotDB):
		ifp = open(f,"r")	
		pathwaysDB = {}	
		ifp.readline()
		lines = ifp.readlines()
		for line in lines:
			tmp = line.strip().split("\t")				
			genes = parseGenes(tmp[3])		
			#print tmp
			#raw_input()
			for g in genes:	
				tmpid = g.replace("_HUMAN","")	
				#print tmpid
				try: 
					tmpid = uniprotDB[tmpid]
				except:
					continue
				for n in tmpid:		
					if not pathwaysDB.has_key(n):
						pathwaysDB[n] = set()
					if tmp[1] == "None":
						pathwaysDB[n].add(tmp[0]) # {GENE:[pathways, p2 ,p3]} #some ids in consPath are none (signalink pathways)
						#print tmp[0]
						#raw_input()
					else:
						pathwaysDB[n].add(tmp[1]) # {GENE:[pathways, p2 ,p3]}		
		return pathwaysDB	
		
	def updateFreqs(self, uid, classe):
		try:
			paths = self.merged[uid]
		except:
			#print "UID not found: %s" % uid
			return		
		for i in paths:			
			self.freqs[i][label(classe)] += 1
	
	def getLogOdd(self, uid):
		try:
			paths = self.merged[uid]
		except:
			return 0
		sumTot = 0.0		
		for i in paths:
			fr = self.freqs[i]
			sumDel = fr[1] / float(fr[0]+fr[1]) 
			sumNeut = fr[0] / float(fr[0]+fr[1])
			sumTot += math.log(sumDel/sumNeut)
		return sumTot
	
	def getPathwayFromUID(self, uid):		
		try:
			return self.merged[uid]			
		except:			
			return []
	
	def mergeDBs(self):
		merged = {}
		for prot in self.uniprotDB.items():
			#print prot					
			try:
				genes = prot[1]
				#print genes			
			except:
				genes = []		
			paths = []			
			for i in genes:
				try:
					paths += self.pathGENEwise[i]
				except:
					continue
			if "None" in paths:
				raise Exception("Wrong pathway identifier!")						
			merged[prot[0]] = paths
		return merged

def parseGenes(s):
	if "," in s:
		return s.strip().split(",")
	else:
		return s.split(" ")
		
def parseGenesUniprot(s, uniprotDB):
	if "," in s:
		r = s.strip().split(",")
	else:
		r = s.split(" ")
	l = set()
	for i in r:
		try:
			tmp = uniprotDB[i.replace("_HUMAN","")]
		except:
			continue
		for j in tmp:
			l.add(j)		
	return l

def parseSubcell(s):
	#print "SUBCELL:",s
	return s.split(";")[0]
	
def parseDomains(s):
	#print "DOMAIN:",s
	return s

def readUniprotTab(f):
	ifp = open(f,"r")
	uniprotDB = {}
	ifp.readline()
	lines = ifp.readlines()
	for line in lines:
		tmp = line.strip().split("\t")		
		#print tmp
		if len(tmp) == 1:
			continue
		if not uniprotDB.has_key(tmp[0]):
			uniprotDB[tmp[0]] = []
		uniprotDB[tmp[0]] = parseGenes(tmp[1])
		#uniprotDB[tmp[0]] = [parseGenes(tmp[1]),parseSubcell(tmp[2]),parseDomains(tmp[3])] #{UID: [GENE, SUCELL, DOMAIN]}
		#raw_input()		
	return uniprotDB
	
def readPathwaysIDwise(f):
	ifp = open(f,"r")	
	pathwaysDB = {}	
	ifp.readline()
	lines = ifp.readlines()
	for line in lines:
		tmp = line.strip().split("\t")
		#print tmp
		if tmp[1] == "None":
			name = tmp[0]
		else:
			name = tmp[1]
		if not pathwaysDB.has_key(name):
			pathwaysDB[name] = []
		pathwaysDB[name] = [tmp[0],parseGenes(tmp[3]),tmp[2]] #{UID: [DESCRIPTION, GENES, SOURCE]}		
	return pathwaysDB
	
def readPathwaysGENEwise(f):
	ifp = open(f,"r")	
	pathwaysDB = {}	
	ifp.readline()
	lines = ifp.readlines()
	for line in lines:
		tmp = line.strip().split("\t")				
		genes = parseGenes(tmp[3])		
		#print tmp
		#raw_input()
		for g in genes:	
			tmpid = g.replace("_HUMAN","")			
			if not pathwaysDB.has_key(tmpid):
				pathwaysDB[tmpid] = []	
			if tmp[1] == "None":
				pathwaysDB[tmpid].append(tmp[0]) # {GENE:[pathways, p2 ,p3]} #some ids in consPath are none (signalink pathways)
				#print tmp[0]
				#raw_input()
			else:
				pathwaysDB[tmpid].append(tmp[1]) # {GENE:[pathways, p2 ,p3]}		
	return pathwaysDB	

def readPROVEAN(filename):
	ifp = open(filename, "r")
	prov = ifp.readlines()	
	ifp.close()	
	print prov.pop(0).split()
	provDB = {}
	neut = 0
	delet = 0
	for row in prov:
		line = row.split()
		#print line	
		if not provDB.has_key(line[0]):
			provDB[line[0]] = []
		provDB[line[0]].append([line[1], float(line[2]), line[-1]])
		if line[-1] == "Disease":
			delet += 1
		elif line[-1] == "Polymorphism":
			neut += 1
		else:
			raise Exception("Expected Disease or Poly here")
	print "Neutral: %d, Disease: %d" % (neut, delet)
	return provDB #{UID:[mutm score, class]}
	
def mergeDBs(uniprot, pathDB):
	merged = {}
	for prot in uniprot.items():
		print prot[0],	
		raw_input()					
		try:
			genes = prot[1][0]
			print genes,			
		except:
			genes = []	
		
		paths = searchPathways1(genes, pathDB)
		
		merged[prot[0]] = paths
	return merged

def main():
	p =Pathways("../databases/consPathDB/uniprotAll.tab","../databases/consPathDB/CPDB_pathways_genes.tab")
	for i in p.pathGENEwise.keys():
		print i
		print "set: ",p.sharePath(i)
		raw_input()
	#print p.merged
	#print len(p.merged)
	return 0

if __name__ == '__main__':
	main()

