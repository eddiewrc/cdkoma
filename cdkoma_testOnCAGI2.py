#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  
#  Copyright 2019 Daniele Raimondi <daniele.raimondi@kuleuven.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import sys, os, cPickle, marshal, time, random, copy, math
import numpy as np
from sources import parseAnnovarMultianno as PAM
from sources.readPhenopedia import readPhenopedia
from sources import pathwayUtils as P
from sources import utils as U
import GraphConv as GCN
from sources.readGWASHist import readGWASHist, getHistCount, crom2int
import sources.Constants as CONST
import torch as t
MIN_REFERENCE_NUM = 1 
STORE_ACTIVATIONS = True 
CAGI_TEST = 2
TESTDB = "DATA/marshalled/cagi"+str(CAGI_TEST)+".multianno.missenseFalse.RegionsNone.m.min"+str(MIN_REFERENCE_NUM)
	
def main(args):
	if len(args) != 2 or "-h" in args[1]: 
		print "USAGE: \n python cdkoma.py NUM_GENES \n"
		print "      NUM_GENES can be 222 or 691"
		print
		return 1
	if args[1] == "222":
		MIN_REFERENCE_NUM = 1
	elif args[1] == "691":
		MIN_REFERENCE_NUM = 0
	else:
		print "Unknown number of genes %s, selecting 222 by default" % args[1]
		MIN_REFERENCE_NUM = 1
	#random.seed(1)
	geneList = sorted(cPickle.load(open("DATA/marshalled/totGeneSet.m.min"+str(MIN_REFERENCE_NUM))))
	#print geneList
	#raw_input()
	print " > Used %d genes" % len(geneList)
	geneDB = {}	
	weightPhenoPGenes, phenoPGenes = readPhenopedia("DATA/databases/phenopediaCrohnGenes/CrohnGenes")	
	#gwasHist = readGWASHist("../databases/gwasCentral/gwasCDstudies.hist.csv")
	#                      0		1		2	  3		4		5		6		7		8		9	 10	  11	12	13		 14		15		16	   
	#db = {(patient:{crom:[(pos, vest, region, gene, vartype, rec, disease, tissues, expr1, expr2, hi, rvis, gdi, metasvm, mcap, domain, gtex), ...]}, label)}
	if MIN_REFERENCE_NUM == 1:
		model = t.load("DATA/models/baseline.trainCAGI4_222genes.t")
	elif MIN_REFERENCE_NUM == 0:
		model = t.load("DATA/models/baseline.trainCAGI4_691genes.t")
	
	wrapper = GCN.NNwrapper(model)
	###################################################################################
	
	analysePreds(TESTDB, geneList, wrapper, weightPhenoPGenes, None, None)#, TESTDB2+".png")
	
	

def analysePreds(TESTDB, geneList, wrapper, weightPhenoPGenes, gwasHist, scaler = None, savefig=None):	
	print "Testing "+TESTDB+" ######################################################################################################"
	t1 = time.time()	
	db = marshal.load(open(TESTDB)) 
	geneDB = {}		
	#                     0		1		2	  3		4		5		6		7		8		9	 10	  11	12	13		 14		15		16	   
	#db = {patient:{crom:[(pos, vest, region, gene, vartype, rec, disease, tissues, expr1, expr2, hi, rvis, gdi, metasvm, mcap, domain, gtex), ...]}}
	t2 = time.time()
	print "Marshalling time: ", t2-t1
		
	HX = {} # {sample:vector}	
	for h in db.items():
		assert len(h[1]) == 2
		sampleName = h[0]
		#print h[0], h[1][1]
		exome = h[1][0]
		label = h[1][1]
		geneDB = scanGenes(exome.items(), geneList) #extracts gene-wise data without processing it
		HX[sampleName] = (buildVectorGeneWise(geneDB, geneList, weightPhenoPGenes, None), label)
	test = HX.keys()		
	x, y = buildFeatVect(HX, test)
	#print scaler
	#raw_input()
	if scaler != None:
		print "Using scaler!"
		#raw_input()
		x = scaler.transform(x)
	shapeX = checkVectors(x,y)
	if STORE_ACTIVATIONS:
		os.system("mkdir -p activations")
		act, yp = wrapper.predict(x, y, batch_size=len(x), GET_ACT = True)
		cPickle.dump(act, open("activations/activationsCAGI"+str(CAGI_TEST)+".minRef"+str(MIN_REFERENCE_NUM)+".cPickle", "w"))
		cPickle.dump(wrapper.model.final[1].weight.data.numpy().tolist(), open("activations/lastLayerCAGI"+str(CAGI_TEST)+".minRef"+str(MIN_REFERENCE_NUM)+".weights.cPickle", "w"))
	else:	
		yp = wrapper.predict(x, y, batch_size=len(x))
	sen, spe, acc, bac, pre, mcc, aucScoreGood, auprc = U.getScoresSVR(yp, y, threshold=None, invert = False, PRINT = True, CURVES = False, SAVEFIG=savefig)	
	U.bestLprecision(yp, y, 5)
	U.bestLprecision(yp, y, 10)
	U.bestLprecision(yp, y, 20)
	U.bestLprecision(yp, y, 30)
	#print yp
	 
def checkVectors(a,b):
	assert b.count(b[0]) != len(b)
	assert len(a) == len(b)
	assert len(a) > 0
	l = len(a[0])
	for e in a:
		assert len(e) == l
		for g in e:
			for i in g:
				#print i	
				if math.isnan(i):				
					raise Exception("NAN found.")
	print "Vectors with %d dimensions." % l
	return len(a), l
	
def buildFeatVect(HX, samples): # {sample:(vector, label)}
	X = []
	Y = []	
	for s in samples:		
		Y.append(HX[s][1])		
		X.append(HX[s][0])
	assert len(X) == len(Y)
	return X, Y

def buildVectorGeneWise(genedb, geneList, weightPhenoPGenes, gwasHist):	
	vect = []
	totWG = sum(weightPhenoPGenes.values())
	for k in geneList:      #0    1     2       3      4       5    6   7    8    9         10    11
		varList = genedb[k]#pos, vest, region, gene, vartype, rec, hi, rvis, gdi, metasvm, mcap, crom
		hist =  countVars(varList)		
		if len(varList) < 1:
			#vect.append(([0]*NUMBINS + [0,0])*2 + [0,0,0,0]+[0]+[0]*10)
			vect.append([0]*len(CONST.TYPES)+[0,0])
			continue
		#tmp = []+hist.values()+	[weightPhenoPGenes[k]/float(totWG)]#, getHistCount(varList[0][11], varList[0][0], gwasHist)]
			
		tmp = []+CONST.getOrderedValues(hist)+	[weightPhenoPGenes[k]/float(totWG), varList[0][7]]#+getVarEffPreds(varList,9)+getVarEffPreds(varList,10)
		vect.append(tmp)
	return vect

def countVars(varlist):
	types = copy.deepcopy(CONST.TYPES)
	for v in varlist: #pos, vest, region, gene, vartype, rec, hi, rvis, gdi, metasvm, mcap
		if "exonic" == v[2] and "synonymous SNV" == v[4]:
			continue
		if "intergenic" == v[2]:
			raiseException("Intergenic should not be present!")#	continue
		types[v[2]] +=1
	return types

def getGeneScores(vl):
	if len(vl) > 0:
		return [vl[0][10], vl[0][11], vl[0][12]]
	else:
		return [0,0,0]
			
def scanGenes(exome, geneList): #extracts gene level info without processin it too much
	geneDB = {}
	for g in sorted(geneList):
		geneDB[g] = []
	for crom in exome:			
		cromName = crom[0]
		varList = crom[1]			
		for var in varList:	
			#print var[3]
			if var[3] not in geneList:
				raise Exception(" >> ERROR: gene %s not found" % var[3])
				continue

			geneDB[var[3]].append(list(var)+[crom2int(crom[0][3:])])
			
	#for i in geneDB.items():
	#	print i
	#raw_input()		
	return geneDB #list of variants foreach gene

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
